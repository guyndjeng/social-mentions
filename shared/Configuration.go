package shared

type Configuration struct {
	TwitterStreamApiUrl string
	MongoDbPath         string
	ReadTimeOut         uint64
	DialTimeOut         uint64
}
