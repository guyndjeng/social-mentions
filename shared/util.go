package shared

import (
	"encoding/json"
	"os"
)

func LoadConfig() (*Configuration, error) {
	var Config Configuration
	file, err := os.Open("config.json")
	if err != nil {
		return nil, err
	}

	decoder := json.NewDecoder(file)
	err = decoder.Decode(&Config)
	if err != nil {
		return nil, err
	}
	return &Config, nil
}
