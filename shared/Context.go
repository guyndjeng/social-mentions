package shared

import "time"

//Session Object
type Session struct {
	Id        int
	Uuid      string
	Email     string
	UserId    int
	CreatedAt time.Time
	IsAuth    bool
}

func (session *Session) String() string {
	session = &Session{
		Id:        time.Now().Nanosecond(),
		CreatedAt: time.Now(),
		IsAuth:    true,
	}
	return string(session.Id)
}

//Application Context Type
type AppContext struct {
}
