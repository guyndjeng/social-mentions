package dataservices

import (
	"gopkg.in/mgo.v2"
	"log"
)

//connects & disconnects to mongo
var db *mgo.Session

func OpenDbConnection() error {
	var err error
	log.Println("dialing mongodb: localhost")
	db, err = mgo.Dial("localhost")
	return err
}

func CloseDbConnection() {
	db.Close()
	log.Printf("closed the database connection")
}

//loads polls from mongo
type mention struct {
	Options []string
}

func LoadKeywords() ([]string, error) {
	var options []string
	iter := db.DB("mentions").C("keywords").Find(nil).Iter()
	var m mention

	for iter.Next(&m) {
		options = append(options, m.Options...)
	}
	iter.Close()
	return options, iter.Err()
}
