package twittervotes

import (
	"io"
	"log"
	"net"
	"net/http"
	"sentiment-analysis/dataservices"
	"strings"
	"sync"
	"time"

	"github.com/joeshaw/envdecode"

	"github.com/dghubble/go-twitter/twitter"
	"github.com/dghubble/oauth1"
)

//TYPES
type tweet struct {
	Text string
}

//FUNCTIONS

//Connects to the Twitter Stream API
var conn net.Conn

func dial(netw, addr string) (net.Conn, error) {
	if conn != nil {
		conn.Close()
		conn = nil
	}
	netc, err := net.DialTimeout(netw, addr, 5*time.Second)
	if err != nil {
		return nil, err
	}
	conn = netc
	return netc, nil
}

//Explicitly closes the connection to the Twitter Stream API
var reader io.ReadCloser

func CloseConn() {
	if conn != nil {
		conn.Close()
	}
	if reader != nil {
		reader.Close()
	}
}

//creates a formatted requests to the twitter api and return the response - per go routine
var (
	authSetupOnce sync.Once
	httpClient    *http.Client
	client        *twitter.Client
	stream        *twitter.Stream
)

func setupTwitterAuth() {
	var ts struct {
		ConsumerKey    string `env:"SA_TWITTER_API_KEY,required"`
		ConsumerSecret string `env:"SA_TWITTER_SECRET,required"`
		AccessToken    string `env:"SA_TWITTER_ACCESS_TOKEN,required"`
		AccessSecret   string `env:"SA_TWITTER_TOKEN_SECRET,required"`
	}
	if err := envdecode.Decode(&ts); err != nil {
		log.Fatalln(err)
	}

	authConfig := oauth1.NewConfig(ts.ConsumerKey, ts.ConsumerSecret)
	token := oauth1.NewToken(ts.AccessToken, ts.AccessSecret)
	httpClient = authConfig.Client(oauth1.NoContext, token)
	client = twitter.NewClient(httpClient)
}

func readFromTwitter(votes chan<- string) {
	authSetupOnce.Do(func() {
		setupTwitterAuth()
	})
	words, err := dataservices.LoadKeywords()

	if err != nil {
		log.Println("Failed to load options ", err)
		return
	}

	params := &twitter.StreamFilterParams{
		Track:         words,
		StallWarnings: twitter.Bool(true),
	}

	demux := twitter.NewSwitchDemux()
	demux.Tweet = func(tweet *twitter.Tweet) {
		for _, word := range words {
			if strings.Contains(strings.ToLower(tweet.Text), strings.ToLower(word)) {
				log.Printf("tweet --> %v\n", tweet.Text)
				votes <- word
			}
		}
	}

	stream, err = client.Streams.Filter(params)
	if err != nil {
		log.Fatalf("%v", err)
	}

	go demux.HandleChan(stream.Messages)
	log.Println("Stopping Stream...")

}

func StartTwitterStream(stopChan <-chan struct{}, votes chan<- string) <-chan struct{} {
	stoppedChan := make(chan struct{}, 1)
	go func() {
		defer func() {
			stoppedChan <- struct{}{}
		}()
		log.Println("Querying Twitter...")
		readFromTwitter(votes)
		for {
			select {
			case <-stopChan:
				log.Println("Stopping Twitter...")
				return
			}
		}
	}()
	return stoppedChan
}
