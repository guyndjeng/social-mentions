package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"sentiment-analysis/dataservices"
	"sentiment-analysis/shared"
	"sentiment-analysis/twittervotes"
	"sync"
	"syscall"
	"time"

	"github.com/nsqio/go-nsq"
)

var initConfigOnce sync.Once
var Configuration *shared.Configuration

func init() {
	var err error
	initConfigOnce.Do(func() {
		Configuration, err = shared.LoadConfig()
		if err != nil {
			log.Fatal("Unable to load the config file")
			return
		}
	})

	fmt.Printf("%v", Configuration.TwitterStreamApiUrl)
}

func publishMentionsToNSQ(votes <-chan string) <-chan struct{} {
	stopChan := make(chan struct{}, 1)

	log.Println("Attempting to publish to queue...")
	pub, err := nsq.NewProducer("localhost:4160", nsq.NewConfig())
	if err != nil {
		log.Fatalf("Error trying to publish to queue %v", err)
		stopChan <- struct{}{}
	}
	go func() {

		for vote := range votes {
			log.Printf("adding vote --> %s", vote)
			pub.Publish("twittermentions", []byte(vote))
		}
		log.Println("Publisher: Stopping")
		pub.Stop()
		log.Println("Publisher Stopped")
		stopChan <- struct{}{}
	}()
	return stopChan
}

func publishMentionsToFirebase(votes <-chan string) <-chan struct{} {
	stopChan := make(chan struct{}, 1)
	return stopChan
}

func main() {
	var stoplock sync.Mutex
	stop := false
	stopChan := make(chan struct{}, 1)
	signalChan := make(chan os.Signal, 1)
	go func() {
		<-signalChan
		stoplock.Lock()
		stop = true
		stoplock.Unlock()
		log.Println("Stopping...")
		stopChan <- struct{}{}
		twittervotes.CloseConn()
	}()
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)

	//opening mongo db
	if err := dataservices.OpenDbConnection(); err != nil {
		log.Fatalln("failed to dial MongoDB:", err)
	}
	defer dataservices.CloseDbConnection()

	votes := make(chan string) // chan for votes
	publisherStoppedChan := publishMentionsToNSQ(votes)
	twitterStoppedChan := twittervotes.StartTwitterStream(stopChan, votes)
	go func() {
		for {
			time.Sleep(1 * time.Minute)
			twittervotes.CloseConn()
			stoplock.Lock()
			if stop {
				stoplock.Unlock()
				return
			}
			stoplock.Unlock()
		}
	}()
	<-twitterStoppedChan
	close(votes)
	<-publisherStoppedChan

}
